export default function Base() {
  return (
    <>
        <body>
            <header>
            </header>
            <main>
                <div id="content">
                    <div class="title">
                        <h3>My Profile</h3>
                    </div>
                    <div class="group">
                        <img src="https://storage.googleapis.com/my_profile_picture/cynthia.jpg" class="author" alt="cynthia"></img>
                        <h4>Cynthia Philander</h4>
                        <h5>Web developer</h5>
                        <p>+6287880210202 | cynthia.philander21@gmail.com | 21 years old</p>
                    </div>
                    <div id="story">
                        <div class="card">
                            <h6>Summary</h6>
                            <p> 
                                Exceptional Information Systems student who aspires to be an innovative programmer and passionate in learning technologies. 
                                Trained at developing websites, especially as a front-end developer. 
                                Aiming to use the skills obtained to help easing people life and develop their business effectively with the help of technologies. 
                                Interested in learning about people's perspectives and business fields for better understanding.
                            </p>
                        </div>
                        <div class="card">
                            <h6>Experiences</h6>
                            <p> 
                                <ul>
                                    <li>
                                        Cloud computing cohort at Bangkit Academy 2023 ~ February 2023-Present
                                    </li>
                                    <li>
                                        Teaching assistant for Databases ~ February 2023-Present
    
                                    </li>
                                    <li>
                                        Staff of Information Technology Development of BEM ~ March 2022-January 2023 
                                    </li>
                                </ul>
                            </p>
                        </div>
                        <div class="card">
                            <h6>Skills</h6>
                            <p> 
                                <ul>
                                    <li>
                                        React.Js
                                    </li>
                                    <li>
                                        Node.Js
                                    </li>
                                    <li>
                                        Next.Js
                                    </li>
                                    <li>
                                        MySQL
                                    </li>
                                    <li>
                                        Springboot
                                    </li>
                                    <li>
                                        Django
                                    </li>
                                    <li>
                                        Google Cloud
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </main>
            <footer>
                Find me on Linkedin: <a href="https://www.linkedin.com/in/cynthia-p-b31528113/">Cynthia's Linkedin</a>
            </footer>
        </body>
        </>
  )
}
